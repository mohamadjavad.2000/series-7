package sbu.cs;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class ConnectedClient implements Runnable{
    final private Socket client;
    private final String filePath;
    private String fileName="";
    private int fileSize;

    public ConnectedClient(String directory, Socket client) {
        this.client = client;
        this.filePath = directory;
    }

    /**
     * if server is on receive and store a file
     */
    @Override
    public void run() {
        if (Server.isTerminated()){
            return;
        }
        try {
            RecStrFile();
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    /**
     * Receive file's name & it's size from client then
     * Receive the file from client & store a copy of it in dataset path
     * @throws IOException ...
     */
    private void RecStrFile() throws IOException {
        DataInputStream dis = new DataInputStream(client.getInputStream());
        Scanner sc = new Scanner(dis);
        PrintWriter out = new PrintWriter(client.getOutputStream());
        if (sc.hasNext())
            fileName =sc.next();
        if (fileName.isEmpty()){
            Server.sOut("Client disconnected unexpectedly:file doesn't exist!");
            client.close();
            return;
        }
        fileSize=sc.nextInt();
        Server.sOut("fileSize :"+fileSize);
//        String cpyFileName = fileName.replaceFirst("(\\b(\\.)\\b)(?!.*\\1)","(copy).");
//        FileOutputStream bos = new FileOutputStream(filePath+"\\"+cpyFileName);
        while (receiver(dis)>0) {
            Server.sOut(fileName + " has data lost. ");
            System.out.println("restarting!");
            out.println("restart");
            out.flush();
        }
        Server.sOut(fileName+" is Received & stored as "+fileName);
        out.println("done");
        out.flush();
//        } else Server.sOut(fileName+" is Received & stored as "+cpyFileName);
        client.close();
    }

    private int receiver(DataInputStream dis) throws IOException {
        FileOutputStream bos = new FileOutputStream(filePath+"/"+fileName);
        byte[] fileBytes = new byte[fileSize];
        int bytesRead,bytesReads=0,m=0;
        while (( bytesRead = dis.read(fileBytes, 0, fileSize))!=-1) {
            bytesReads+=bytesRead;
            m=(fileSize-bytesReads);
            Server.sOut(fileName+"\tBytes remained is " +m);
            bos.write(fileBytes, 0, bytesRead);
            if (m<=0)
                return m;
        }
        return m;
    }
}
