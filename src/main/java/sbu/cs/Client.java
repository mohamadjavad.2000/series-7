package sbu.cs;

import java.io.*;
import java.net.ConnectException;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    private static Socket socket;

    /**
     * Create a socket and connect to server
     * then send fileName and after that send file to be saved on server side.
     *  in here filePath == fileName
     *
     * @param args a string array with one element
     */
    public static void main(String[] args) throws IOException{
        String filePath = args[0];      // "sbu.png" or "book.pdf"
        socketConnector();
        OutputStream out=socket.getOutputStream();
        PrintWriter printWriter=new PrintWriter(out);
        int fileSize = fileSize(filePath);
        if (fileSize==-1)
            return;
        BufferedInputStream bis = new BufferedInputStream(new FileInputStream(filePath));
        printWriter.println(filePath+"\n"+fileSize);
        System.out.println("fileSize :"+fileSize);
        printWriter.flush();
        byte[] fileBytes = new byte[fileSize];
        Scanner restartCMDReceiver =new  Scanner(socket.getInputStream());
        int bytesRead = bis.read(fileBytes, 0, fileSize);
        send(filePath, fileBytes, out ,bytesRead);
        while ( restartCMDReceiver.next()== "restart" && socket.isConnected()){
            if (!send(filePath, fileBytes, out ,bytesRead))
                break;
        }
        System.out.println(filePath+" is sent.");
        out.close();
        bis.close();
        socket.close();
    }

    private static boolean send(String filePath, byte[] fileBytes, OutputStream out, int bytesRead) throws IOException {
        System.out.println(filePath+" is sending...");
        System.out.println(bytesRead+" Bytes sent");
        try {
            out.write(fileBytes, 0, bytesRead);
            out.flush();
        }catch (Exception e){
            return false;
        }
        return true;
    }

    /**
     * check if file exist & return its size
     * @param filePath file which will copied
     * @return if exist size of file and else -1
     * @throws IOException closing socket
     */
    private static int fileSize(String filePath) throws IOException {
        File file=new File(filePath);
        if(!file.exists()){
            System.out.println(filePath+" not find.");
            socket.close();
            return -1;
        }
        return (int) file.length();
    }

    /**
     * create client socket : host-IP4 hostIP & port-number port
     *  hostIP : default 127.0.0.1
     *  port : default 8080
     * user can change values by inputting "IP4:PORT"
     */
    private static void socketConnector() {
        String hostIP="127.0.0.1"; //default IP4
        int port=8081;
//        int port=-1;
//        String IP4Pattern ="((25[0-5]|2[0-4][0-9]|[0-9]|[1-9][0-9]|1[0-9][0-9])\\.){3}" +
//                "(25[0-5]|2[0-4][0-9]|[0-9]|[1-9][0-9]|1[0-9][0-9])";
//        String portPattern = "[1-9][0-9][0-9][0-9]";
//        Scanner userIn= new Scanner(System.in);
//        while (port==-1){
            try {
//                port=8080; //default port
//                System.out.println("Enter server \"IP4-Address:PortNumber\"" +
//                        "\for default 127.0.0.1:8080 input any character :");
//                if (userIn.hasNext(IP4Pattern+":" +portPattern)){
//                    String temp= userIn.next(IP4Pattern+":" +portPattern);
//                    hostIP=temp.split(":")[0];
//                    try {
//                        port=Integer.parseInt(temp.split(":")[1]);
//                    }
//                    catch (NumberFormatException e){
//                        System.out.print("Invalid port ! Enter a port number :");
//                        if (userIn.hasNextInt())
//                            port= userIn.nextInt();
//                        else{
//                            port=-1;
//                        }
//                    }
//                }else {
//                    userIn.nextLine();
//                }
//                while (port<0 || port>9999 ){
//                    System.out.print("Invalid port ! Enter a port number :");
//                    if (userIn.hasNextInt())
//                        port= userIn.nextInt();
//                }
                System.out.println("Trying to connect to "+hostIP+":"+port);
                socket = new Socket(hostIP, port);
            }catch (ConnectException e){
                System.out.println(e.getMessage());
//                port=-1;
            } catch (IOException e) {
                e.printStackTrace();
            }
//        }
        System.out.println("Socket connected to "+hostIP+":"+port+" successfully.");
    }
}
