package sbu.cs;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.Scanner;

public class Server {
    private static ServerSocket serverSocket;
    /**
     * Create a server here and wait for connection to establish,
     *  then get file name
     *  and after that get file and save it in the given directory
     *
     * @param args an string array with one element
     * @throws IOException serverSocket
     */
    public
    static void main(String[] args) throws IOException {
        String dir=directoryMaker(args);
//        startServer(getPort(),dir);
        startServer(8081,dir);
    }

//    /**
//     * get listening port from admin & return it
//     */
//    private static int getPort() {
//        Scanner admin = new Scanner(System.in);
//        System.out.print("ENTER PORT :");
//        int port = admin.nextInt();
//        while (port<0 || port>9999 ){
//            System.out.print("Is it really true ?!?\nENTER PORT :");
//            port= admin.nextInt();
//        }
//        return port;
//        return 8080;
//    }

    /**
     * Create a server & wait for clients on port
     * every client has a separated thread for ConnectedClient service
     * listening & accepting is in a true while & only by exception or calling terminate methode will be stopped
     *
     * @param port listening port
     * @param dir database address
     * @throws IOException serverSocket
     */
    private static void startServer(int port,String dir) throws IOException {
        serverSocket =new ServerSocket(port);
        System.out.println("Server is listening on port "+port);
        int counter=0;
        while (!isTerminated()){
            ConnectedClient client = new ConnectedClient(dir,serverSocket.accept());
            new Thread(client).start();
            System.out.println("Client "+counter+" connected: "+client.getClass());
            counter++;
        }
        System.out.println("Terminated");
        serverSocket.close();
    }

    /**
     * make directory if it don't exist
     * -if arg is empty dir="server-database"
     * else dir = arg[0]
     *
     * @param args an String arr with 01 element
     * @return String directory address
     */
    private static String directoryMaker(String[] args) {
        // below is the name of directory which you must save the file in it
        String dir = "server-database";// default: "server-database"
        if (args.length>0)
            dir = args[0];
        File directory=new File(dir);
        while (!directory.exists()) {
            System.out.println("Creating database directory :" + dir);
            if (directory.mkdir())
                System.out.println("Creating directory failed!");
            else System.out.println("Directory created.");
        } System.out.println("Database directory exists.");
        return dir;
    }

    /**
     * check if any thread closed server socket
     * @return is server socket closed?
     */
    synchronized static boolean isTerminated(){
        return serverSocket.isClosed();
    }

    /**
     * synchronized static close server socket
     * @throws IOException If this socket has an associated channel then the channel is closed as well.
     */
    synchronized static void terminate() throws IOException {
        serverSocket.close();
    }

    /**
     * synchronized static System.out.println
     * @param massage cmdline output
     */
    synchronized static void sOut(String massage){
        System.out.println(massage);
    }
}
